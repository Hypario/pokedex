import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PokedexComponent} from "./pokemons/pokedex/pokedex.component";
import {TeamComponent} from "./team/team.component";
import {LoginComponent} from "./auth/login/login.component";
import {LogoutComponent} from "./auth/logout/logout.component";

const routes: Routes = [
  { path: 'pokemons', component: PokedexComponent },
  { path: 'pokemons/:id',  component: PokedexComponent},
  { path: 'login', component: LoginComponent},
  { path: 'team', component: TeamComponent},
  { path: 'logout', component: LogoutComponent},
  { path: '', redirectTo: 'pokemons', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
