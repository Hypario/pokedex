import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthToken} from "./models/auth.model";
import {BehaviorSubject, map, Observable, ReplaySubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private authUrl = "http://app-ec21e68e-3e55-42d7-b1ae-3eef7507a353.cleverapps.io"

  private _isLoggedIn = new BehaviorSubject<boolean>(false);

  public isLoggedIn(): Observable<boolean> {
    return this._isLoggedIn;
  }

  constructor(private httpClient: HttpClient) {}

  public login(username: string, password: string): Observable<void> {
    return this.httpClient.post<AuthToken>(`${this.authUrl}/auth/login`, {
      "email": username,
      "password": password
    }).pipe(map((authToken) => {
      localStorage.setItem("tokens", JSON.stringify(authToken));
      this._isLoggedIn.next(true);

      // refresh token before it expires
      setInterval(() => {
        this.refreshToken();
      }, authToken.expires_in * 1000);
    }));
  }

  public logout(): void {
    localStorage.removeItem("tokens");
    this._isLoggedIn.next(false);
  }

  public getToken(): string {
    return JSON.parse(localStorage.getItem('tokens') as string).access_token;
  }

  private refreshToken(): void {
    const tokens: AuthToken = JSON.parse(localStorage.getItem("tokens") as string);

    this.httpClient.post<AuthToken>(`${this.authUrl}/auth/refresh`, {refresh_token: tokens.refresh_token})
      .subscribe((authToken) => {
        localStorage.setItem("token", authToken.access_token);
      });
  }

}
