import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginValid = true;
  public username = '';
  public password = '';

  private readonly returnUrl: string;

  // TODO : Add a register component and form
  constructor(private _route: ActivatedRoute, private router: Router, private authService: AuthService) {
    this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/team';
  }

  public onSubmit(): void {
    this.authService.login(this.username, this.password).subscribe(
      () => {
        this.router.navigate([this.returnUrl]);
      }
    )
  }

  ngOnInit(): void {
  }

}
