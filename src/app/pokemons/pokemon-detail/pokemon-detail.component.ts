import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PokemonDetail} from "../models/pokemon.model";
import {PokemonService} from "../pokemon.service";

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit {

  public pokemon?: PokemonDetail;

  public audioElement?: HTMLAudioElement;

  public shakeClass: string = '';

  constructor(private route: ActivatedRoute, public pokemonService: PokemonService) {}

  getPokemon(id: number) {
    if (!id) { return; }
    this.pokemonService.getPokemon(id).subscribe(pokemon => {
      this.pokemon = pokemon;
      this.play(`assets/audio/${this.pokemon.id}.mp3`); // play audio when pokemon is loaded
    });
  }

  onAudioClick() {
    if (this.pokemon)
      this.play(`assets/audio/${this.pokemon.id}.mp3`);
  }

  ngOnInit(): void {
    this.audioElement = new Audio();
    this.route.params.subscribe(params => {
      this.getPokemon(Number(params["id"]));
      if (this.pokemon) {
        this.play(`assets/audio/${this.pokemon.id}.mp3`);
      }
    });
  }

  private play(src: string) {
    if (!this.audioElement) return;
    this.audioElement.src = src;
    this.audioElement.play().then(() => {
      this.shakeClass = 'shake';
      setTimeout(() => {
        this.shakeClass = '';
      }, 1000);
    }).catch((err) => { }); // ignore error
  }

}
