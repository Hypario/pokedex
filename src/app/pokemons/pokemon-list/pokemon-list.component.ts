import {Component, OnInit} from '@angular/core';
import {PokemonService} from "../pokemon.service";
import {Pokemon, PokemonDetail} from "../models/pokemon.model";
import {AuthService} from "../../auth/auth.service";
import {find, map} from "rxjs";

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {

  public isLoggedIn?: boolean;

  public pokemons: Pokemon[] = [];

  public searching: string = '';

  private offset: number = 0;
  private limit: number = 20;

  constructor(private pokemonService: PokemonService, private authService: AuthService) { }

  onScroll() {
    this.pokemonService.getPokemons(this.offset += this.limit, this.limit).subscribe(
      (result) => {
        this.pokemons = this.pokemons.concat(result.data);
      });
  }

  onSearch() {
    if (this.searching != '') {
      this.pokemonService.searchPokemon(this.searching).subscribe(
        (result) => {
          this.pokemons = result.data;
        });
    } else {
      this.pokemonService.getPokemons(this.offset = 0, this.limit).subscribe(
        (result) => {
          this.pokemons = result.data;
        });
    }
  }

  addToTeam(id: number) {
    this.pokemonService.addToTeam(id)
  }

  inTeam(id: number): boolean {
    return this.pokemonService.inTeam(id);
  }

  removeFromTeam(id: number) {
    this.pokemonService.removeFromTeam(id)
  }

  ngOnInit(): void {
    this.pokemonService.getPokemons(this.offset, this.limit).subscribe(
      (result) => {
        this.pokemons = result.data;
      }
    );
    this.authService.isLoggedIn().subscribe((result) => {
      this.isLoggedIn = result;
    });
  }

}
