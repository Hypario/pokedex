import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, empty, forkJoin, map, mergeMap, Observable, of, switchMap} from "rxjs";
import {PagedData} from "./models/data.model";
import {Pokemon, PokemonDetail} from "./models/pokemon.model";
import {AuthService} from "../auth/auth.service";


@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private isAuthenticated = false;

  private apiUrl = "http://app-ec21e68e-3e55-42d7-b1ae-3eef7507a353.cleverapps.io"; // https://pokeapi.co/api/v2/pokemon/"

  private _team: BehaviorSubject<PokemonDetail[]> = new BehaviorSubject<PokemonDetail[]>([]);
  public readonly pokemonTeam: Observable<PokemonDetail[]> = this._team.asObservable();

  public getPokemons(offset: number = 0, limit: number = 20): Observable<PagedData<Pokemon>> {
    return this.httpClient.get<PagedData<Pokemon>>(`${this.apiUrl}/pokemons?offset=${offset}&limit=${limit}`);
  }

  public getPokemon(id: number): Observable<PokemonDetail> {
    return this.httpClient.get<PokemonDetail>(`${this.apiUrl}/pokemons/${id}`);
  }

  public addToTeam(id: number): void {
    if (!this.authService.isLoggedIn) {
      return
    }
    this.getTeamAsId()?.subscribe(team => {
      if (team.length < 6) {
        team.push(id);
        this.httpClient.put(`${this.apiUrl}/trainers/me/team`, team, {
          headers: {
            'Authorization': `Bearer ${this.authService.getToken()}`
          }
        }).subscribe(() => {
          this.getTeam().subscribe(team => {
            this._team.next(team);
          })
        });
      }
    });
  }

  public removeFromTeam(id: number) {
    if (!this.authService.isLoggedIn) {
      return
    }
    this.getTeamAsId()?.subscribe((team: number[]) => {
      if (team.length < 6 && team.find((value) => value === id)) {
        team = team.filter((value) => value !== id);
        this.httpClient.put(`${this.apiUrl}/trainers/me/team`, team, {
          headers: {
            'Authorization': `Bearer ${this.authService.getToken()}`
          }
        }).subscribe(() => {
          this.getTeam().subscribe(team => {
            this._team.next(team);
          })
        });
      }
    });
  }

  public inTeam(id: number): boolean {
    return this._team.getValue().find(value => value.id === id) !== undefined;
  }

  private getTeam(): Observable<PokemonDetail[]> {
    if (this.isAuthenticated) {
      return this.httpClient.get<number[]>(`${this.apiUrl}/trainers/me/team`, {
        headers: {
          "Authorization": `Bearer ${this.authService.getToken()}`
        }
      }).pipe(
        switchMap((ids: number[]) => {
          if (ids.length === 0) return of([]);
          return forkJoin(ids.map(id => this.getPokemon(id)));
        })
      )
    }
    return new Observable<PokemonDetail[]>();
  }

  private getTeamAsId(): Observable<number[]> | null {
    if (this.isAuthenticated) {
      return this.httpClient.get<number[]>(`${this.apiUrl}/trainers/me/team`, {
        headers: {
          "Authorization": `Bearer ${this.authService.getToken()}`
        }
      })
    } else
      return null;
  }

  constructor(private httpClient: HttpClient, private authService: AuthService) {
    this.getTeam().subscribe(team => this._team.next(team));
    this.authService.isLoggedIn().subscribe(value => this.isAuthenticated = value);
  }

  public searchPokemon(searching: string) {
    return this.httpClient.get<PagedData<Pokemon>>(`${this.apiUrl}/pokemons?search=${searching}`);
  }
}
