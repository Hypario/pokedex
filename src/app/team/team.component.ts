import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth/auth.service";
import {environment} from "../../environments/environment";
import {Pokemon, PokemonDetail} from "../pokemons/models/pokemon.model";
import {PokemonService} from "../pokemons/pokemon.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  public team: PokemonDetail[] = [];

  constructor(private authService: AuthService, private pokemonService: PokemonService) {}

  ngOnInit(): void {
    this.pokemonService.pokemonTeam.subscribe(team => {
      this.team = team;
    });
  }

}
